import React from "react";
import ReactDOM from "react-dom";
import Metronome from "./components/Metronome";
import BPMCounter from "./components/BPMCounter";
import "./index.css";

class App extends React.Component {
  state = { model_bpm: undefined, user_bpm: undefined, game_state: "WAIT", errorMessage: null };

  setUserBPM(user_bpm) {
    this.setState({ user_bpm });
    this.updateGameState();
  }

  updateGameState() {
    let s = this.state.game_state;

    if (s === "WAIT") {
      let model_bpm = Math.trunc(Math.random() * 150 + 50);
      let timeout = (1000 * 60 * 8) / model_bpm;

      this.setState({ model_bpm, game_state: "COUNT_IN" });
      setTimeout(this.updateGameState.bind(this), timeout);
    }

    if (s === "COUNT_IN") {
      this.setState({ game_state: "TAP" });
    }

    if (s === "TAP") {
      this.setState({ game_state: "WAIT" });
    }
  }

  render() {
    return (
      <div className="app">
        <div className="circle">
          <div className="game-items">
            {this.state.game_state === "WAIT" ? <h1>Tapswell</h1> : null}
            {this.state.game_state === "WAIT" ? (
              <button
                id="start_button"
                onClick={this.updateGameState.bind(this)}
              >
                Click to Start
              </button>
            ) : null}

            {
              this.state.game_state === "ERROR" ?
                <p class="error-message">
                  {this.state.errorMessage}
                </p>
                : null
            }

            {this.state.game_state === "COUNT_IN" ? (
              <Metronome bpm={this.state.model_bpm} onError={(errorMessage) => { this.setState({ game_state: "ERROR", errorMessage }) }} />
            ) : null}

            {this.state.game_state === "TAP" ? (
              <BPMCounter setUserBPM={this.setUserBPM.bind(this)} />
            ) : null}

            {this.state.game_state === "WAIT" && this.state.user_bpm ? (
              <div>
                <p>
                  <strong>Given BPM</strong>: {this.state.model_bpm}
                </p>
                <p>
                  <strong>Your BPM</strong>: {this.state.user_bpm.toFixed(2)}
                </p>
              </div>
            ) : null}
          </div>
        </div>
        <div className="instruction">
          <h1>Instruction</h1>
          <ol>
            <li>Hit Start</li>
            <li>Listen to the 2 bars of count-in</li>
            <li>
              Tap quarter notes in the same tempo as the count-in for 4 bars
            </li>
          </ol>
        </div>
      </div>
    );
  }
}

ReactDOM.render(<App />, document.querySelector("#root"));
