import React from "react";
import "./BPMCounter.css";

class BPMCounter extends React.Component {
  constructor(props) {
    super(props);

    this.state = { bpm: 0 };
    this.tap = 0;
    this.startTime = null;
  }

  onClick(e) {
    if (this.tap === 0) {
      this.tap += 1;
      this.startTime = e.timeStamp;
    } else {
      this.tap += 1;
      let averageTapLengthInMilliseconds =
        (e.timeStamp - this.startTime) / (this.tap - 1);

      this.setState({ bpm: (60 * 1000) / averageTapLengthInMilliseconds });
    }
  }

  render() {
    if (this.tap === 16) this.props.setUserBPM(this.state.bpm);
    return (
      <div>
        <button className="tap-button" onClick={this.onClick.bind(this)}>
          Tap
        </button>
      </div>
    );
  }
}

export default BPMCounter;
