import React from "react";

class Metronome extends React.Component {
  constructor(props) {
    super(props);

    this.defaultVolume = 0.98;
    this.defaultCount = 8;

    this.count = 0;

    try {
      this.audioContext = new AudioContext();
    }

    catch {
      props.onError("The browser couldn't make the audio context.");
      return;
    }

    this.osc = this.audioContext.createOscillator();
    this.env = this.audioContext.createGain();

    this.bpm = props.bpm;

    //These are in Seconds
    this.lastNoteTime = this.audioContext.currentTime;
    this.noteInterval = this.calculateIntervalInSeconds(this.bpm);
    this.noteLength = 50 / 1000;

    this.attackTime = 2 / 1000;
    this.sustainTime = 20 / 1000;
    this.decayTime = 10 / 1000;

    //These are in Milliseconds
    this.schedulerInterval = 50;
    this.schedulerLength = 150;

    //Connect the audio nodes
    this.osc.connect(this.env);
    this.env.connect(this.audioContext.destination);
    this.env.gain.value = 0;
    this.osc.start();
    setInterval(this.scheduler.bind(this), this.schedulerInterval);
  }

  calculateIntervalInSeconds(bpm) {
    return 60 / bpm;
  }

  scheduler() {
    const startTime = this.audioContext.currentTime;
    let nextTime = this.lastNoteTime + this.noteInterval;

    while (
      nextTime <= startTime + this.schedulerLength / 1000 &&
      this.count < this.defaultCount
    ) {
      //Queue notes
      this.env.gain.setValueAtTime(0, nextTime);

      this.env.gain.linearRampToValueAtTime(
        this.defaultVolume,
        nextTime + this.attackTime
      );

      this.env.gain.linearRampToValueAtTime(
        this.defaultVolume,
        nextTime + this.attackTime + this.sustainTime
      );

      this.env.gain.linearRampToValueAtTime(
        0,
        nextTime + this.attackTime + this.sustainTime + this.decayTime
      );

      this.lastNoteTime = nextTime;
      nextTime += this.noteInterval;
      this.count += 1;
    }
  }

  render() {
    return <div>This is metronome at {this.bpm} BPM</div>;
  }
}

export default Metronome;
