# Tapswell

A game / tool that checks your timing accuracy.

You can play the live demo at <https://carbaa.gitlab.io/tapswell/>

## Getting Started

- `npm install` to install dependencies
- `npm start` to run this on a development server

## How to play

After two bars of count-in, click the 'Tap' button for 4 bars at a tempo closest to the tempo of the count-in.

BPM is randomized from 50 to 200.
